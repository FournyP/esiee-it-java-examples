package instruments;

public class Percussion implements Instrument {

    @Override
    public void play() {
        System.out.println("Poum");
    }

    @Override
    public String what() {
        return "Percussion";
    }

    @Override
    public void adjust() {
        System.out.println("Percussion adjusted");
    }
}
