package instruments;

public interface Instrument {
    
    /**
     * Method to play the instrument
     */
    public void play();

    /**
     * Method to return the instrument name
     * 
     * @return
     */
    public String what();

    /**
     * Method to adjust the instrument
     */
    public void adjust();
}
