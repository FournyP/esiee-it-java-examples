package instruments;

public class Wind implements Instrument {

    @Override
    public void play() {
        System.out.println("Fiouuuuuuu");
    }

    @Override
    public String what() {
        return "Wind";
    }

    @Override
    public void adjust() {
        System.out.println("Wind adjusted");
    }
}
