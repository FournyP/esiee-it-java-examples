package tools;

public class Calcul {
    
    private float firstNumber;

    private float secondNumber;

    /**
     * Default constructor
     */
    public Calcul()
    {
        firstNumber = 0;
        secondNumber = 0;
    }

    /**
     * Constructor
     * 
     * @param firstNumber To initialize firstNumber member
     * @param secondNumber To initialize secondNumber member
     */
    public Calcul(float firstNumber, float secondNumber) 
    {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    /**
     * Method to make addition between firstNumber and secondNumber
     * 
     * @return firstNumber + secondNumber
     */
    public float addition() 
    {
        return firstNumber + secondNumber;
    }

    /**
     * Method to make substraction between firstNumber and secondNumber
     * 
     * @return firstNumber - secondNumber
     */
    public float soustraction()
    {
        return firstNumber - secondNumber;
    }

    /**
     * Method to make multiplication between firstNumber and secondNumber
     * 
     * @return firstNumber * secondNumber
     */
    public float multiplication()
    {
        return firstNumber * secondNumber;
    }

    /**
     * Method to make division between firstNumber and secondNumber
     * 
     * @return firstNumber / secondNumber
     */
    public float division()
    {
        try
        {
            return firstNumber / secondNumber;
        }
        catch (ArithmeticException ae)
        {
            return 0;
        }
    }

    /**
     * Getter of firstNumberMember
     * 
     * @return firstNumber member
     */
    public float getFirstNumber() {
        return this.firstNumber;
    }

    /**
     * Setter of firstNumber member
     * 
     * @param firstNumber New firstNumber
     */
    public void setFirstNumber(float firstNumber) {
        this.firstNumber = firstNumber;
    }

    /**
     * Getter of secondNumber
     * 
     * @return secondNumber member
     */
    public float getSecondNumber() {
        return this.secondNumber;
    }

    /**
     * Setter of secondNumber member
     * 
     * @param secondNumber New secondNumber
     */
    public void setSecondNumber(float secondNumber) {
        this.secondNumber = secondNumber;
    }
}
