package tools;

import java.security.InvalidParameterException;
import java.util.regex.Pattern;

public class MesDates {

    /**
     * Function to format french date into us date
     * @param dateFr
     * @return
     */
    public static String dateFRtoUS(String dateFr) {
        
        if (dateFr == null)
            throw new NullPointerException("dateFr parameter cannot be null");

        Pattern pattern = Pattern.compile("^\\d{2}/\\d{2}/\\d{4}$");

        if (!pattern.matcher(dateFr).matches()) 
            throw new InvalidParameterException("Date given is not a french date");

        String[] dateSplit = dateFr.split("/");

        return String.join("-", dateSplit[1], dateSplit[0], dateSplit[2]);
    }

    /**
     * Function to format us date into french date
     */
    public static String dateUStoFR(String dateUs) {
        
        if (dateUs == null)
            throw new NullPointerException("dateUs parameter cannot be null");

        Pattern pattern = Pattern.compile("^\\d{2}-\\d{2}-\\d{4}$");

        if (!pattern.matcher(dateUs).matches())
            throw new InvalidParameterException("Date given is not an us date");

        String[] dateSplit = dateUs.split("-");

        return String.join("/", dateSplit[1], dateSplit[0], dateSplit[2]);
    }
}
