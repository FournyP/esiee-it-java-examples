package main;

import tools.FileReader;
import tools.MesDates;

import java.util.ArrayList;

import instruments.Percussion;
import instruments.Wind;
import instruments.Woodwind;
import modeles.Visite;
import modeles.Planning;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        //Tests for modeles
        Visite v1 = new Visite();

        Visite v2 = new Visite("Pierro", "13/12/2000", 20);

        System.out.println(v1);
        System.out.println(v2);

        String usDate = MesDates.dateFRtoUS(v2.getDate());
        String frenchDate = MesDates.dateUStoFR(usDate);

        System.out.println(usDate);
        System.out.println(frenchDate);

        ArrayList<Visite> visites = new ArrayList<Visite>();
        visites.add(v1);
        visites.add(v2);

        Planning p1 = new Planning(visites);

        System.out.println(p1);

        //Tests for instruments
        Wind wind = new Wind();
        Woodwind woodwind = new Woodwind();
        Percussion percussion = new Percussion();

        wind.play();
        woodwind.play();
        percussion.play();

        //Tests for FileReader
        FileReader.ReadFile("./test.txt");
    }
}
