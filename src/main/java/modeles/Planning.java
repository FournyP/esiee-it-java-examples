package modeles;

import java.util.ArrayList;

/**
 * @author Fourny Pierre
 */
public class Planning {
    
    private ArrayList<Visite> visites;

    /**
     * Constructor of Planning
     */
    public Planning() 
    {
        visites = null;
    }

    /**
     * Constructor of Planning
     * @param visites To initialize visites memnber
     */
    public Planning(ArrayList<Visite> visites) 
    {
        this.visites = visites;
    }

    /**
     * Getter of visites member
     * @return visites member
     */
    public ArrayList<Visite> getVisites() {
        return this.visites;
    }

    /**
     * Setter of visites member
     * @param visites New visites member
     */
    public void setVisites(ArrayList<Visite> visites) {
        this.visites = visites;
    }
    
    /**
     * Method to append visite in visites member
     * @param visiteToAdd
     */
    public void AjoutVisite(Visite visiteToAdd) {
        this.visites.add(visiteToAdd);
    }

    /**
     * Method to remove visite in visites members
     * @param studentName
     */
    public void SupprVisite(String studentName) {

        for (Visite visite : visites) {
            
            if (visite.getStudentName().equals(studentName)) {
                visites.remove(visite);
            }
        }
    }


    @Override
    public String toString() {
        return "{" +
            " visites='" + getVisites() + "'" +
            "}";
    }

}
