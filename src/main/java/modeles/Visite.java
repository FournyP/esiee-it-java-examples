package modeles;

/**
 * @author Fourny Pierre
 */
public class Visite {

    private String studentName;

    private String date;

    private int note;

    /**
     * Default constructor
     */
    public Visite() {
        studentName = null;
        date = null;
        note = 0;
    }

    /**
     * Constructor to initialize all members
     * 
     * @param studentName To initialize studentName member
     * @param date        To initialize date member
     * @param note        To initialize not member
     */
    public Visite(String studentName, String date, int note) {
        this.studentName = studentName;
        this.date = date;
        this.note = note;
    }

    /**
     * Getter of studentName member
     * 
     * @return String : studentName
     */
    public String getStudentName() {
        return this.studentName;
    }

    /**
     * Setter of studentName member
     * 
     * @param studentName New value of studentName member
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    /**
     * Getter of date member
     * 
     * @return String : date
     */
    public String getDate() {
        return this.date;
    }

    /**
     * Setter of date member
     * 
     * @param date New value of date member
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * Getter of note member
     * 
     * @return int : note
     */
    public int getNote() {
        return this.note;
    }

    /**
     * Setter of note
     * 
     * @param note New value of note member
     */
    public void setNote(int note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "{" + " studentName='" + getStudentName() + "'" + ", date='" + getDate() + "'" + ", note='" + getNote()
                + "'" + "}";
    }

}
